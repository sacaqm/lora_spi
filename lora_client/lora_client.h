#ifndef LORACLIENT_H
#define LORACLIENT_H

#include <stdint.h>
#include <zephyr/device.h>

#define LORA_CLIENT_PAYLOAD_SIZE 200

// Initializes the LoRa client with SPI
void lora_SPI_init(void);

// Sends data via SPI to the LoRa module
void lora_SPI_send(const uint8_t *data, size_t data_len);

// Receives data from the LoRa module via SPI
size_t lora_SPI_receive(uint8_t *data, size_t max_len);

#endif

/**
 * LORA client for the Nordic nRF9160DK
 * based on the LORA_PHY and LORAWAN libraries
 *
 * https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/zephyr/connectivity/lora_lorawan/index.html
 *
 * Carlos.Solans_AT_CERN
 * May 2023
 */
