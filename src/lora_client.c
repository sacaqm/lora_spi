#include <zephyr/drivers/spi.h>
#include <zephyr/drivers/lora.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <string.h>
#include <lora_client/lora_client.h>
#include <stdlib.h>

const struct device *lora_SPI_dev;

void lora_SPI_init(void) {
	lora_SPI_dev = DEVICE_DT_GET(DT_NODELABEL(spi3));
    if (!lora_SPI_dev) {
        printk("LORA_SPI: SPI device not found\n");
    }
    if (!device_is_ready(lora_SPI_dev)) {
        printk("LORA_CLIENT: %s Device not ready", lora_SPI_dev->name);
        return;
	}
}
void lora_SPI_send(const uint8_t *data, size_t data_len) {
    printk("LORA_SPI: send\n");

    if (spi_write(lora_SPI_dev, data, data_len) != 0) {
        printk("LORA_SPI: SPI write failed\n");
    } else {
        printk("LORA_SPI: Data sent via SPI\n");
    }
}

size_t lora_SPI_receive(uint8_t *data, size_t max_len) {
    printk("LORA_SPI: receive\n");

    int ret = spi_read(lora_SPI_dev, data, max_len);
    if (ret < 0) {
        printk("LORA_SPI: SPI read failed\n");
        return 0;
    } else {
        printk("LORA_SPI: Data received via SPI\n");
        return ret;
    }
}