#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <lora_client/lora_client.h>
#include <zephyr/kernel.h>
#include <string.h>
#include <zephyr/drivers/spi.h>

void main(void) {
    printk("Welcome to lora_SPI example application\n");
    printk("Wait 5 seconds...\n");
    k_sleep(K_SECONDS(5));

    // Initialize LoRa_SPI client with the SPI device
    lora_SPI_init();

    // A sample data to send
    uint8_t data[] = {0x01, 0x02, 0x03, 0x04};

    // Send data using LoRa_SPI client
    lora_SPI_send(data, sizeof(data));

    while (1) {
        k_sleep(K_SECONDS(1));
    }
}